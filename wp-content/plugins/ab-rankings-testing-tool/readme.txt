=== SEO A/B Split Testing: Google Search Engine Optimization (SEO) Plugin ===
Contributors: nomadicmedia
Tags: seo plugin, a/b testing, seo, seo test, seo tool, title tag, meta description, meta tags, seo optimization, a/b test, seo rank, seo score
Requires at least: 4.6
Tested up to: 5.0.3
Requires PHP: 5.3
Stable tag: 0.9
Version: 0.9.1
License: GNU General Public License v2.0 or later

Increase your SEO traffic: Plugin to split test different titles, meta descriptions to improve your Google search SEO.

== Description ==

Boost Your Wordpress SEO Traffic

= Optimize your post titles and descriptions for better click through rates =

Imagine if you could get 25% more clicks from your existing Google SEO rankings? By running SEO experiments to improve your articles' headlines, title tags and meta descriptions to be more compulsive, adding a call to action or simply better target your keywords you can significantly increase traffic from your existing search engine rankings.

A/B Rankings will help you test new variations of your site's titles, layout and content in order to optimize your Google SERP snippet, making your blog or website site look more valuable to the searcher and increasing your organic SEO click through rates (CTR).

> <strong>A/B Rankings SEO Plugin</strong><br>
> A/B Rankings is a standalone SEO tool that integrates with all CMS including WordPress. You must have an A/B Rankings account in order to take advantage of this plugin. <a href="https://abrankings.com/pricing/" rel="friend" title="A/B Rankings Plans">Click here to create your FREE account.</a>

= How to start SEO split testing =

1. Install this plugin
2. Sign up at A/B Rankings
3. Connect your Google Search Console (Webmaster Tools) account so we can track changes in your SEO ranks
4. Create a new test on abrankings.com and the SEO test's changes will be deployed instantly to your Wordpress site

Changes made to your site will be visible to all users immediately, so from the very first crawl of your test page Google will see your changes and we will start tracking their impact on your SEO positions.

= Test new content ideas, widgets and copy =

How would your pages perform if you were to update old content, or add an extra 500 words of copy to cover new SEO keywords?
Before you start an expensive content creation campaign, why not test the impact of changes to your site's SEO performance by updating your pages and monitoring the impact on your traffic and revenue?

You might also want to test:

* Adding additional content to pages to increase keyword coverage and long tail SEO rankings
* Adding schema.org JSON-LD structured data to a page for featured snippets in Google's SERPs
* Altering keyword anchors on internal links to improve a major page's SEO positions and traffic
* Adding a linked table of contents to an in-depth guide to optimize for inline sitelinks in Google's search engine result pages
* Adding boilerplate SEO copy to pages to see if you can increase ranking keyword combinations on Google
or anything else you can think of!

== Installation ==

1. Install A/B Rankings SEO Testing Plugin either via the WordPress.org plugin repository or by uploading the files to your server.
2. Activate A/B Rankings SEO Testing Plugin.
3. Navigate to the A/B Rankings tab at the bottom of your admin menu and enter your A/B Rankings API Key, then choose your site's url to connect your A/B Rankings account to your WordPress site.
4. Your SEO tests' changes will be automatically downloaded and displayed on your site

== Frequently Asked Questions ==

= I downloaded this plugin but don't have an A/B Rankings account. How can I get one? =

You can sign up for an A/B Rankings account by <a href="https://abrankings.com/pricing/" rel="friend" title="A/B Rankings Plans and Pricing">visiting our Pricing page</a> and choosing the account that best suits your needs. Once you've signed up you have all you need to start testing your search engine optimization strategies.

== Screenshots ==


== Notes ==

A/B Rankings is an advanced <a href="https://abrankings.com" rel="friend" title="A/B Rankings">SEO testing tool</a>. No other WordPress SEO plugin or WordPress title or meta description optimization tool offers the ability to measure the increase in traffic due to your search engine optimization (SEO) efforts.

== Changelog ==

= 0.8.9 =
* Resolved intermittent upload issues

= 0.8.8 =
* Improved compatibility with older PHP versions

= 0.8.6 =
* Fixed typos in SEO test dashboard

= 0.8.5 =
* Added diagnostics.

= 0.7.5 =
* Initial release.
