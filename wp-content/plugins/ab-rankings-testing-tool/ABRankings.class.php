<?php
//namespace App\Providers;
/**
 * Created by PhpStorm.
 * User: alldocube
 * Date: 19/04/2018
 * Time: 11:20
 */
class ABRankings
{
    private $token;
    private $domain = 'https://abrankings.com';


    public function __construct()
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            if($_SERVER['HTTP_HOST']=='wp.test') $this->domain='http://abranker.test';
        }

    }

    // Get a list of the users' site names and IDs
    public function getSites($token='')
    {
        $response = Requests::get($this->domain . '/api/sites?api_token=' . $token);
//        print_r($response);
        $sites=json_decode($response->body,TRUE);

        return $sites;
    }
    // Download all tests for a site to store locally
    public function getTests($id,$token='')
    {
        $tests_url=$this->domain.'/api/tests?api_token='.$token.'&abr_id='.$id;
//        $response=file_get_contents($tests_url);
        $response=Requests::get($tests_url);
//        print_r($response);
        $GLOBALS['http_response']= (int) $response->status_code;
//        echo $tests_url;
//        echo $response;
        $tests=json_decode($response->body,true);
        return $tests;
    }

    // Download test for a single url
    public function getTestsForUrl($url, $id = 0, $type='')
    {
        $ch = curl_init();

        $test_url=$this->domain.'/test?url='.urlencode($url).'&abr_id='.$id.'&type='.$type;

        curl_setopt($ch, CURLOPT_URL, $test_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");


        $headers = array();
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $headers[] = "User-Agent: ".$_SERVER['HTTP_USER_AGENT'];
        } else $headers[] = "User-Agent: Unknown";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
            print_r(curl_errno($ch));
        }
        curl_close ($ch);
//        echo $result;
        $test=json_decode($result, TRUE);

        return $test;
    }

    // return the HTML we provide altered by a test
    public function alterHTML($html, $test, $tokens = array())
    {
        $dom=new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);

        $xpath=new DOMXPath($dom);

        // grab any extra data from the DOM - tokens should already include token / meta info
        if ((isset($test['grabbers'])) && (is_array($test['grabbers'])) ) {
            foreach ($test['grabbers'] as $grabber) {
                foreach ($xpath->query($grabber['xpath']) as $element) {
                    if ($grabber['collect'] == 'attr') $token[$grabber['name']] = $element->getAttribute($grabber['attr']);
                    else $tokens[$grabber['name']] = $element->nodeValue;
                }
            }
        }
        // Make changes to the DOM based on our test values

        if ((isset($test['alters'])) && (is_array($test['alters'])) ) {
            foreach ($test['alters'] as $alter) {

                $elements = $xpath->query($alter['xpath']);

                $alter['newValue'] = (string) $this->replaceTokens($alter['newValue'], $tokens);

                if ($elements->length == 0) {
                    // create the element! How do we do this from a CSS selector/xpath? HMMMMMMMMMMMMMMMMMMMMMMMMMMM
                    // do we really want to do this for anything except 'there is no meta/title tag'? Surely most operations
                    // will be append/prepend/replacetext? EG append '<meta name="robots" content="noindex,follow">' to HEAD?
                    // ok then, lets hardcode for now:
                    if (trim($alter['selector']) == 'meta[name=\'description\']') {
                        $head = $dom->getElementsByTagName('head')->item(0);
//                        print_r($head);
                        $meta = $dom->createElement('meta');
                        $meta->setAttribute('name', 'description');
                        $meta->setAttribute('content', $alter['newValue']);
                        $head->appendChild($meta);
                    }
                } else {
                    foreach ($elements as $element) {
                        if ($alter['change'] == 'text') $element->textContent = $alter['newValue'];
                        if ($alter['change'] == 'html') $element->nodeValue = $alter['newValue'];
                        if ($alter['change'] == 'prepend') $element->nodeValue = $alter['newValue'] . $element->nodeValue;
                        if ($alter['change'] == 'append') $element->nodeValue .= $alter['newValue'];
                        if ($alter['change'] == 'attr') $element->setAttribute($alter['attr'], $alter['newValue']);

                    }
                }
                // every change to DOM means we need to regen our xpath
                $xpath = new DOMXPath($dom);
            }
        }
        return $dom->saveHTML();
    }

    public function checkTest($url, $abr_id, $GoogleCache = FALSE)
    {
//        echo $url . ':'.$abr_id;
        $test = $this->getTestsForUrl($url,$abr_id);
        $found=0;
        $checks=0;
//        print_r($test);
        if (is_array($test['alters'])) {
            $ch = curl_init();
            if ($GoogleCache) {
                // Add proxy stuff later..
                $url="http://webcache.googleusercontent.com/search?q=cache:".urlencode($url)."&cd=4&hl=en&ct=clnk&gl=uk";
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cache-Control: no-cache','Pragma: no-cache','User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'));


            $html=curl_exec($ch);
            //        echo $html;
            $doc=new DOMDocument();

            libxml_use_internal_errors(true);
            $doc->loadHTML($html);

            $xpath=new DOMXPath($doc);

            foreach ($test['alters'] as $alter) {
                $elements = $xpath->query($alter['xpath']);
                $match = preg_replace('/{(.*)}/', '(.*)', $alter['newValue']);
//            echo $match;
                foreach ($elements as $element) {
//                echo $element->textContent;
                    if ($alter['change'] == 'text') $assert = preg_match("/^$match$/", $element->textContent);
                    // really not sure the below will work
                    if ($alter['change'] == 'html') $assert = preg_match("/^$match$/", (string)$element->nodeValue);
                    if ($alter['change'] == 'prepend') $assert = preg_match("/^$match(.*)$/", (string)$element->nodeValue);
                    if ($alter['change'] == 'append') $assert = preg_match("/^(.*)$match)$/", (string)$element->nodeValue);
                    if ($alter['change'] == 'attr') $assert = preg_match("/$match/", $element->getAttribute($alter['attr']));
                    if ($assert == TRUE) $found++;
                    $checks++;
                }
//            echo "\n";
            }
        } //else echo "No tests found";
//        echo "$found found from $checks checks";
        // do we want to return the test itself?
        return $checks>0 ?  $found/$checks : 0;
    }

    // basic token replacement
    public function replaceTokens($string, $tokens)
    {
        preg_match_all('/{{\s*(.*)\s*}}/U',$string,$matches);

        $replacements=array();

        foreach($matches[1] as $key => $token) {
            $token=trim($token);
            if (isset($tokens[trim($token)])) {
                $replacements[ $matches[0][$key] ] = $tokens[trim($token)];
            }
        }
//        print_r($tokens);
//        print_r($replacements);
        $string = strtr($string,  $replacements);
//        echo $string;
        return $string;
    }


}