<?php
/* ------------------------------------------------------------------------------------
*  COPYRIGHT AND TRADEMARK NOTICE
*  Copyright 2008-2019 Arnan de Gans. All Rights Reserved.
*  ADROTATE is a registered trademark of Arnan de Gans.

*  COPYRIGHT NOTICES AND ALL THE COMMENTS SHOULD REMAIN INTACT.
*  By using this code you agree to indemnify Arnan de Gans from any
*  liability that might arise from it's use.
------------------------------------------------------------------------------------ */

$banners = $groups = $schedules = $queued = $unpaid = 0;
$banners = $wpdb->get_var("SELECT COUNT(*) FROM `{$wpdb->prefix}adrotate` WHERE `type` = 'active';");
$groups = $wpdb->get_var("SELECT COUNT(*) FROM `{$wpdb->prefix}adrotate_groups` WHERE `name` != '';");
$schedules = $wpdb->get_var("SELECT COUNT(*) FROM `{$wpdb->prefix}adrotate_schedule` WHERE `name` != '';");
$queued = $wpdb->get_var("SELECT COUNT(*) FROM `{$wpdb->prefix}adrotate` WHERE `type` = 'queue' OR `type` = 'reject';");
$data = get_option("adrotate_advert_status");

// Review URL
$license = get_site_option('adrotate_activate');
$license = (!$license) ? 'single' : strtolower($license['type']);
?>

<div id="dashboard-widgets-wrap">
	<div id="dashboard-widgets" class="metabox-holder">
		<div id="left-column" class="ajdg-postbox-container">

			<div class="ajdg-postbox">				
				<h2 class="ajdg-postbox-title"><?php _e('At a Glance', 'adrotate-pro'); ?></h2>
				<div id="currently" class="ajdg-postbox-content">
					<table width="100%">
						<thead>
						<tr class="first">
							<td width="50%"><strong><?php _e('Your setup', 'adrotate-pro'); ?></strong></td>
							<td width="50%"><strong><?php _e('Adverts that need you', 'adrotate-pro'); ?></strong></td>
						</tr>
						</thead>
						
						<tbody>
						<tr class="first">
							<td class="first b"><a href="admin.php?page=adrotate-ads"><?php echo $banners; ?> <?php _e('Active Adverts', 'adrotate-pro'); ?></a></td>
							<td class="b"><a href="admin.php?page=adrotate-ads"><?php echo $data['expiressoon'] + $data['expired']; ?> <?php _e('(Almost) Expired', 'adrotate-pro'); ?></a></td>
						</tr>
						<tr>
							<td class="first b"><a href="admin.php?page=adrotate-groups"><?php echo $groups; ?> <?php _e('Groups', 'adrotate-pro'); ?></a></td>
							<td class="b"><a href="admin.php?page=adrotate-ads"><?php echo $data['error']; ?> <?php _e('Have errors', 'adrotate-pro'); ?></a></td>
						</tr>
						<tr>
							<td class="first b"><a href="admin.php?page=adrotate-schedules"><?php echo $schedules; ?> <?php _e('Schedules', 'adrotate-pro'); ?></a></td>
							<td class="b"><?php echo ($adrotate_config['enable_advertisers'] == 'Y') ? '<a href="admin.php?page=adrotate-ads&view=queue">'.$queued.' '.__('Queued', 'adrotate-pro').'</a>' : '&nbsp;'; ?></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="ajdg-postbox">
				<h2 class="ajdg-postbox-title"><?php _e('AdRotate Professional', 'adrotate-pro'); ?></h2>
				<div id="services" class="ajdg-postbox-content">
					<p><strong><?php _e('Support AdRotate Professional', 'adrotate-pro'); ?></strong></p>
					<p><?php _e('Consider writing a review or making a donation if you like the plugin or if you find the plugin useful. Thanks for your support!', 'adrotate-pro'); ?></p>
					<p><center><a class="button-primary" target="_blank" href="https://ajdg.solutions/product/adrotate-pro-<?php echo $license; ?>/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=review_link#tab-reviews">Write review on ajdg.solutions</a> <a class="button-secondary" target="_blank" href="https://wordpress.org/support/plugin/adrotate/reviews/?rate=5#new-post">Write review on WordPress.org</a></center></p>

					<p><strong><?php _e('Plugins and services', 'adrotate-pro'); ?></strong></p>
					<table width="100%">
						<tr>
							<td width="33%">
								<div class="ajdg-sales-widget" style="display: inline-block; margin-right:2%;">
									<a href="https://ajdg.solutions/product/adrotate-html5-setup-service/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=service-6" target="_blank"><div class="header"><img src="<?php echo plugins_url("/images/offers/html5-service.jpg", dirname(__FILE__)); ?>" alt="HTML5 Advert setup" width="228" height="120"></div></a>
									<a href="https://ajdg.solutions/product/adrotate-html5-setup-service/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=service-6" target="_blank"><div class="title"><?php _e('HTML5 Advert setup', 'adrotate-pro'); ?></div></a>
									<div class="sub_title"><?php _e('Professional service', 'adrotate-pro'); ?></div>
									<div class="cta"><a role="button" class="cta_button" href="https://ajdg.solutions/product/adrotate-html5-setup-service/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=service-6_buy" target="_blank">Only &euro; 22,50 p/ad</a></div>
									<hr>
									<div class="description"><?php _e('Did you get a HTML5 advert and can’t get it to work in AdRotate Pro? I’ll install and configure it for you.', 'adrotate-pro'); ?></div>
								</div>							
							</td>
							<td width="33%">
								<div class="ajdg-sales-widget" style="display: inline-block; margin-right:2%;">
									<a href="https://ajdg.solutions/product/wordpress-maintenance-and-updates/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=service-3" target="_blank"><div class="header"><img src="<?php echo plugins_url("/images/offers/wordpress-maintenance.jpg", dirname(__FILE__)); ?>" alt="WordPress Maintenance" width="228" height="120"></div></a>
									<a href="https://ajdg.solutions/product/wordpress-maintenance-and-updates/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=service-3" target="_blank"><div class="title"><?php _e('WP Maintenance', 'adrotate-pro'); ?></div></a>
									<div class="sub_title"><?php _e('Professional service', 'adrotate-pro'); ?></div>
									<div class="cta"><a role="button" class="cta_button" href="https://ajdg.solutions/product/wordpress-maintenance-and-updates/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=service-3_buy" target="_blank">Starting at &euro; 22,50</a></div>
									<hr>								
									<div class="description"><?php _e('Get all the latest updates for WordPress and plugins. Maintenance, delete spam and clean up files.', 'adrotate-pro'); ?></div>
								</div>
							</td>
							<td>
								<div class="ajdg-sales-widget" style="display: inline-block;">
									<a href="https://ajdg.solutions/product/woocommerce-single-page-checkout/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=woo-2" target="_blank"><div class="header"><img src="<?php echo plugins_url("/images/offers/single-page-checkout.jpg", dirname(__FILE__)); ?>" alt="WooCommerce Single Page Checkout" width="228" height="120"></div></a>
									<a href="https://ajdg.solutions/product/woocommerce-single-page-checkout/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=woo-2" target="_blank"><div class="title"><?php _e('Single Page Checkout', 'adrotate-pro'); ?></div></a>
									<div class="sub_title"><?php _e('WooCommerce Plugin', 'adrotate-pro'); ?></div>
									<div class="cta"><a role="button" class="cta_button" href="https://ajdg.solutions/product/woocommerce-single-page-checkout/?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=woo-2_buy" target="_blank">Only &euro; 10,-</a></div>
									<hr>
									<div class="description"><?php _e('Merge your cart and checkout pages into one single page in seconds with no setup required at all.', 'adrotate-pro'); ?></div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>

		</div>
		<div id="right-column" class="ajdg-postbox-container">

			<div class="ajdg-postbox">				
				<h2 class="ajdg-postbox-title"><?php _e('News & Updates', 'adrotate-pro'); ?></h2>
				<div id="news" class="ajdg-postbox-content">
					<p><center><a href="https://www.arnan.me?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=news_link" title="Visit Arnan's website" target="_blank"><img src="<?php echo plugins_url("/images/buttons/1.png", dirname(__FILE__)); ?>" alt="Arnan de Gans website" /></a><a href="https://ajdg.solutions?pk_campaign=adrotatepro&pk_keyword=info_page&pk_content=news_link" title="Visit the AJdG Solutions website" target="_blank"><img src="<?php echo plugins_url("/images/buttons/2.png", dirname(__FILE__)); ?>" alt="AJdG Solutions website" /></a><a href="https://www.twitter.com/arnandegans" title="Arnan de Gans on Twitter" target="_blank"><img src="<?php echo plugins_url("/images/buttons/4.png", dirname(__FILE__)); ?>" alt="Arnan de Gans on Twitter" /></a><a href="https://arnandegans.tumblr.com" title="AJdG Solutions on Tumblr" target="_blank"><img src="<?php echo plugins_url("/images/buttons/3.png", dirname(__FILE__)); ?>" alt="AJdG Solutions on Tumblr" /></a></center></p>

					<?php wp_widget_rss_output(array(
						'url' => 'http://ajdg.solutions/feed/', 
						'items' => 5, 
						'show_summary' => 1, 
						'show_author' => 0, 
						'show_date' => 1)
					); ?>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="clear"></div>
<p><?php echo adrotate_trademark(); ?></p>