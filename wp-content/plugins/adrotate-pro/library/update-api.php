<?php
/* ------------------------------------------------------------------------------------
*  COPYRIGHT AND TRADEMARK NOTICE
*  Copyright 2008-2019 Arnan de Gans. All Rights Reserved.
*  ADROTATE is a trademark of Arnan de Gans.

*  COPYRIGHT NOTICES AND ALL THE COMMENTS SHOULD REMAIN INTACT.
*  By using this code you agree to indemnify Arnan de Gans from any
*  liability that might arise from it's use.
------------------------------------------------------------------------------------ */

/*-------------------------------------------------------------
 Name:      AJdG Solutions Update and Support Library
 Version:	1.5
---- CHANGELOG ------------------------------------------------
1.5.2 - 11/january/2020
	* Replaced 12 hour schedule for update checks with transient (similar to version 1.5)
	* Transient is valid for 12 hours

1.5.1 - 1/december/2019
	* Replaced $response['new_version'] with $response['version'] in adrotate_update_check()

1.5 - 20/october/2019
	* Redone most code
	* More efficient/lean data for showing updates
	* 12 Hour caching cycle with transients (to reduce API requests)

1.4 - 1/july/2019
	* Now uses API 7

1.3.2 - 1/july/2019
	* Improved error checking
	* Improved backwards compatibility for older PHP versions

1.3.1 - 27/june/2019
	* Added error checking for update and info requests

1.3 - 29/may/2019
	* All new basic_check request
	* All new plugin_information request

1.2.4 - 21/feb/2019
	* Fixed slug not listed correctly on line 72

1.2.3 - 4/apr/2018
	* Dropped support for 101 licenses

1.2.2 - 28/feb/2016
	* changed unserialize() into maybe_unserialize() on line 49

1.2.1 - 5/6/2015
	* Added extra check if plugin exists in update array
-------------------------------------------------------------*/

/*-------------------------------------------------------------
 Name:      adrotate_update_check
 Purpose:   Check for new version and do basic_check
-------------------------------------------------------------*/
function adrotate_update_check($transient) {
	if(empty($transient->checked)) {
		return $transient;	
	}

	$plugin_version = get_plugins();
	if(!array_key_exists('adrotate-pro/adrotate-pro.php', $plugin_version)) { 
		return $transient;
	}

	$plugin_version = $plugin_version['adrotate-pro/adrotate-pro.php']['Version'];

	$response = get_transient('ajdg_update_adrotatepro');
	if($response == false) {
		$license = adrotate_get_license();

		$args = array(
			'slug' => "adrotate-pro", 
			'version' => $plugin_version, 
			'instance' => (is_array($license)) ? $license['instance'] : '', 
			'platform' => get_option('siteurl')
		);
		$response = wp_remote_post('https://ajdg.solutions/api/updates/7/', adrotate_prepare_request('basic_check', $args));

		if(!is_wp_error($response) AND isset($response['response']['code']) AND $response['response']['code'] == 200 AND !empty($response['body'])) {
 			$response = json_decode($response['body'], 1);
			set_transient('ajdg_update_adrotatepro', $response, 43200);

			if($response['created'] > 0 AND is_array($license)) {
				$license['created'] = $response['created'];
				if(adrotate_is_networked()) {
					update_site_option('adrotate_activate', $license);
				} else {
					update_option('adrotate_activate', $license);
				}
			}
		}
	}

	if($response) {
		$result = new stdClass();
		$result->slug = $response['slug'];
		$result->plugin = "adrotate-pro/adrotate-pro.php";
		$result->new_version = $response['version'];
		$result->tested = $response['tested'];
		$result->package = $response['download_url'];
		$result->upgrade_notice = "<strong>Update Summary:</strong> ".$response['upgrade_note'];
		$result->icons = array('1x' => $response['icons']['low'], '2x' => $response['icons']['high']);

		if(version_compare($plugin_version, $response['version'], '<') AND version_compare($response['requires_wp'], get_bloginfo('version'), '<=')) {
			$transient->response[$result->plugin] = $result;
			$transient->checked[$result->plugin] = $response['version'];
		} else {
			$transient->no_update[$result->plugin] = $result;
		}
	}

	return $transient;
}

/*-------------------------------------------------------------
 Name:      adrotate_get_plugin_information
 Purpose:   Grab info from API for popup screen in dashboard (plugin_information)
-------------------------------------------------------------*/
function adrotate_get_plugin_information($false, $action, $args) {
	if($action !== 'plugin_information') {
		return false;
	}

	if($args->slug != "adrotate-pro") {
		return $false;
	}
		
	$response = get_transient('ajdg_update_adrotatepro');
	if($response == false) {
		$license = adrotate_get_license();
		$plugin_version = get_plugins();
		$plugin_version = $plugin_version['adrotate-pro/adrotate-pro.php']['Version']; 

		$args = array(
			'slug' => "adrotate-pro", 
			'version' => $plugin_version, 
			'instance' => (is_array($license)) ? $license['instance'] : '', 
			'platform' => get_option('siteurl')
		);
		$response = wp_remote_post('https://ajdg.solutions/api/updates/7/', adrotate_prepare_request($action, $args));
	 	
		if(!is_wp_error($response) AND isset($response['response']['code']) AND $response['response']['code'] == 200 AND !empty($response['body'])) {
	 		$response = json_decode($response['body'], 1);
			set_transient('ajdg_update_adrotatepro', $response, 43200);
		}
	}
 
	if($response) {
		$result = new stdClass();
		$result->name = $response['name'];
		$result->slug = $response['slug'];
		$result->version = $response['version'];
		$result->tested = $response['tested'];
		$result->requires = $response['requires_wp'];
		$result->requires_php = $response['requires_php'];
		$result->author = $response['author'];
		$result->homepage = $response['plugin_url'];
		$result->download_link = $response['download_url'];
		$result->last_updated = $response['release_date'];
		$result->active_installs = $response['active_installs'];
		$result->donate_link = $response['donate_link'];
		$result->banners = array('low' => $response['banners']['low'], 'high' => $response['banners']['high']);
		$result->sections = array(
			'description' => stripslashes($response['sections']['description']), 
			'changelog' => $response['sections']['changelog'],
		);
		
		return $result;
	}

	return false;
}

/*-------------------------------------------------------------
 Name:      adrotate_update_finished
 Purpose:   Clear transient after update to not confuse WP with old data
-------------------------------------------------------------*/
function adrotate_update_finished($upgrader_object, $options) {
	if($options['action'] == 'update' AND $options['type'] === 'plugin')  {
		delete_transient('ajdg_update_adrotatepro');
	}
}

/*-------------------------------------------------------------
 Name:      adrotate_prepare_request
 Purpose:   Set Headers and prepare update request
-------------------------------------------------------------*/
function adrotate_prepare_request($action, $args) {
	return array(
		'headers' => array('Accept' => 'multipart/form-data'),
		'body' => array('action' => $action, 'request' => serialize($args)),
		'user-agent' => 'AdRotate Pro/' . $args['version'] . '; ' . $args['platform'],
		'sslverify' => false,
	);	
}
?>